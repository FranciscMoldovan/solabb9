#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFF_SIZE 10

pthread_t producer_th, consumer_th1, consumer_th2;
sem_t sema_only_one, sema_no_data, sema_full_of_data, sema_print;
int buff[BUFF_SIZE];
int buff_start = 0, buff_end = 0;

void *producer_func(void *arg)
{
    for(;;)
    {
        usleep(rand()%10 * 1000);

        int producedNumber = rand()%100;
       

        sem_wait(&sema_full_of_data); // Increment the full_of_data indicator
        sem_wait(&sema_only_one); // Check for exclusivity
            buff[buff_end] = producedNumber;
           

        sem_wait(&sema_print);
        printf(">>> PRODUCED: %d POS=%d\n", producedNumber, buff_end);
        sem_post(&sema_print);

        if(buff_end < BUFF_SIZE -1)
            buff_end++;
        else
            buff_end=0;

        sem_post(&sema_only_one);
        sem_post(&sema_no_data);
    }
    return NULL;
}

void *consumer_func(void *arg)
{
    for(;;)
    {
        usleep(rand()%10 * 80000);
        sem_wait(&sema_no_data); // Wait here, as there's nothing to read
        sem_wait(&sema_only_one); // Get exclusive access
        sem_wait(&sema_print);
            printf("\t<<<CONSUMED: %d POS=%d\n", buff[buff_start], buff_start);
        sem_post(&sema_print);
          
        if(buff_start < BUFF_SIZE -1)
            buff_start++;
        else
            buff_start=0;

        sem_post(&sema_only_one); 
        sem_post(&sema_full_of_data);
    }
    return NULL;
}


int main()
{   
    sem_init(&sema_only_one,     0,         1); // ONLY one allowed in
    sem_init(&sema_no_data,      0,         0); // Initially NO DATA
    sem_init(&sema_full_of_data, 0, BUFF_SIZE); // Initially ALL EMPTY
    sem_init(&sema_print,        0,         1);

    pthread_create(&producer_th , NULL, producer_func, NULL);
    pthread_create(&consumer_th1, NULL, consumer_func, NULL);
    pthread_create(&consumer_th2, NULL, consumer_func, NULL);

    pthread_join(consumer_th1, NULL);
    pthread_join(consumer_th2, NULL);
    pthread_join(producer_th, NULL);
return 0;
}
